#./dev-env/bin/activate

def valid_input(message, options):
    while True:
        response = int(input(message))
        if response in options:
            return response
        else:
            print("Entrada inválida.")


def define_t():
    print("Quanto tempo dura o passe?")
    t = (valid_input
        ("Insira um número em forma de dígto entre 1 e 24.\n", 
        [num for num in range(1, 25)]
        ))
    return int(t)


def define_q():
    print("Quantos jogadores estão em quadra?")
    q = (valid_input
        ("Insira um número em forma de dígito entre 2 e 5.\n", 
        [num for num in range(2, 6)]
        ))
    return q


def define_x():
    print("Qual o tempo de reposicionamento dos jogadores?")
    x = int(valid_input
        ("Insira um número em forma de dígito entre 1 e 4.\n", 
        [num for num in range(1, 5)]
        ))
    return x


def define_result():
    t = define_t()
    q = define_q()
    x = define_x()
    
    repositining_players = 0
    free_players_list= []
    for i in range(t):
        available_players = q - 1 - repositining_players
        free_players_list.append(available_players)
        if available_players > 1:
            repositining_players +=1
        else:
            pass

    print("O somatório de jogadores livres para cada passe no treino é de: "
       f"{sum(free_players_list)}")


define_result()

