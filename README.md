Desafio para estágio na empresa Vortex em parceria com a UNIFOR.
Código para definir o somatório de jogadores livres para cada passe no treino. Como definido segundo documento em https://drive.google.com/file/d/11E66kcGfqnCu266VHg4MJSPm1yN7Zxuj/view

Para a versão em Python
Para executar o arquivo nesta versão, é necessário ter instalado Python3.9 e executar o arquivo training.py que está na pasta /python-version.
Comando para execução "python3 training.py" estando na pasta /python-version via terminal.

Para a versão em JavaScript e HTML
Basta abrir o arquivo index.html no seu navegador.
